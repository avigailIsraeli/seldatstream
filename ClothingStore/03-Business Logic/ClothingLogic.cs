﻿
using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class ClothingLogic : BaseLogic {
        public List<ClothingModel> GetAllClothes() {
            return DB.Clothes.Select(c => new ClothingModel {
                id = c.ClothID,
                category = new CategoryModel { id = c.Category.CategoryID, name = c.Category.CategoryName },
                company = new CompanyModel { id = c.Company.CompanyID, name = c.Company.CompanyName },
                type = new TypeModel { id = c.Type.TypeID, name = c.Type.TypeName },
                price = c.Price,
                discount = c.Discount,
                image = c.Image
            }).ToList();
        }

        public ClothingModel saveImage(int clothingId, string imageName) {
            DB.Clothes.FirstOrDefault(c => c.ClothID == clothingId).Image = imageName;
            DB.SaveChanges();
            return DB.Clothes.Include("Categories").Include("Types").Include("Companies")
                 .Where(c => c.ClothID == clothingId).Select(c => new ClothingModel {
                     id = c.ClothID,
                     category = new CategoryModel { id = c.Category.CategoryID, name = c.Category.CategoryName },
                     company = new CompanyModel { id = c.Company.CompanyID, name = c.Company.CompanyName },
                     type = new TypeModel { id = c.Type.TypeID, name = c.Type.TypeName },
                     price = c.Price,
                     discount = c.Discount,
                     image = c.Image
                 }).FirstOrDefault();
        }

        public ClothingModel GetOneCloth(int id) {
            return DB.Clothes.Where(c => c.ClothID == id).Select(c => new ClothingModel {
                id = c.ClothID,
                category = new CategoryModel { id = c.Category.CategoryID, name = c.Category.CategoryName },
                company = new CompanyModel { id = c.Company.CompanyID, name = c.Company.CompanyName },
                type = new TypeModel { id = c.Type.TypeID, name = c.Type.TypeName },
                price = c.Price,
                discount = c.Discount,
                image = c.Image
            }).FirstOrDefault();
        }

        public ClothingModel AddCloth(ClothingModel model) {
            Cloth cloth = new Cloth {
                CategoryID = model.category.id,
                CompanyID = model.company.id,
                TypeID = model.type.id,
                Price = model.price,
                Discount = model.discount,
                Image = model.image
            };
            DB.Clothes.Add(cloth);
            DB.SaveChanges();
            model.id = cloth.ClothID;
            return model;
        }

        public ClothingModel UpdateFullCloth(ClothingModel model) {
            Cloth cloth = new Cloth { ClothID = model.id };
            DB.Clothes.Attach(cloth);
            cloth.Category = DB.Categories.FirstOrDefault(c => c.CategoryID == model.category.id);
            cloth.Type = DB.Types.FirstOrDefault(c => c.TypeID == model.type.id);
            cloth.Company = DB.Companies.FirstOrDefault(c => c.CompanyID == model.company.id);
            cloth.Price = model.price;
            cloth.Discount = model.discount;
            if (model.image != null)
                cloth.Image = model.image;
            DB.SaveChanges();
            return model;
        }

        public ClothingModel UpdatePartialCloth(ClothingModel model) {
            Cloth cloth = DB.Clothes.FirstOrDefault(c => c.ClothID == model.id);
            if (cloth == null) {
                return null;
            }

            if (model.category != null) {
                cloth.Category.CategoryID = model.category.id;
                cloth.Category.CategoryName = model.category.name;
            }
            else {
                model.category.id = cloth.Category.CategoryID;
                model.category.name = cloth.Category.CategoryName;
            }

            if (model.company != null) {
                cloth.Company.CompanyID = model.company.id;
                cloth.Company.CompanyName = model.company.name;
            }
            else {
                model.company.id = cloth.Company.CompanyID;
                model.company.name = cloth.Company.CompanyName;
            }

            if (model.type != null) {
                cloth.Type.TypeID = model.type.id;
                cloth.Type.TypeName = model.type.name;
            }
            else {
                model.type.id = cloth.Type.TypeID;
                model.type.name = cloth.Type.TypeName;
            }

            if (model.price != 0)
                cloth.Price = model.price;
            else
                model.price = cloth.Price;

            if (model.discount != null)
                cloth.Discount = model.discount;
            else
                model.discount = cloth.Discount;

            if (model.image != null)
                cloth.Image = model.image;
            else
                model.image = cloth.Image;

            DB.SaveChanges();
            return model;
        }

        public void DeleteCloth(int id) {
            Cloth cloth = new Cloth { ClothID = id };
            DB.Clothes.Attach(cloth);
            DB.Clothes.Remove(cloth);
            DB.SaveChanges();
        }
    }
}


