﻿
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Seldat {
    public class AdminLogic : BaseLogic {
        public bool CheckAdmin(AdminModel admin) {
            string password=MD5Hash(admin.password);
            return DB.Admins.Any(a => a.AdminName == admin.name && a.AdminPassword == password);
        }

            public static string MD5Hash(string password) {
                MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            byte[] result = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

                //get hash result after compute it
                //md5.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < result.Length; i++) {
                    //change it into 2 hexadecimal digits
                    //for each byte
                    strBuilder.Append(result[i].ToString("x2"));
                }

                return strBuilder.ToString();
            }
        }
    }
