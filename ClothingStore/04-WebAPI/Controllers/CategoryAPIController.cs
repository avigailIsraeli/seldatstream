﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat.Controllers {

    [EnableCors("*", "*", "*")]
    public class CategoryAPIController : ApiController {
        private CategoriesLogic logic = new CategoriesLogic();

        [HttpGet]
        [Route("api/categories")]
        public HttpResponseMessage GetCategories() {
            try {
                List<CategoryModel> categories = logic.GetAllCategories();
                return Request.CreateResponse(HttpStatusCode.OK, categories);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

       

        [HttpGet]
        [Route("api/categories/{id}")]
        public HttpResponseMessage GetCloth(int id) {
            try {
                List<ClothingModel> clothes = logic.GetClothesByCategories(id);
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
